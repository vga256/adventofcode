﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode
{
    public class assignment4
    {
        private List<string> _listLines = new List<string>();
        private List<LogEntry> _logEntries = new List<LogEntry>();
        private List<GuardData> _guardLogs = new List<GuardData>();

        DateTime currentGuardStartTime = new DateTime();
        //DateTime currentGuardWakeTime = new DateTime();
        DateTime currentGuardSleepTime = new DateTime();

        //entry data for each guard
        //according to the date they were
        //on shift, and the minutes they spent asleep
        private class LogEntry
        {
            public DateTime TimeStamp;
            public int GuardID;
            public string GuardLog;
        }

        private class GuardData
        {
            public int GuardID;
            public int MinutesAsleep;
            public int[] MinuteByMinute = new int[60];
        }

        public assignment4()
        {
            string currentLine;
            //read raw text data from file
            //and push it into a list of strings
            System.IO.StreamReader assignmentinput;
            assignmentinput = new System.IO.StreamReader(@"input/day4input.txt");
            while ((currentLine = assignmentinput.ReadLine()) != null)
            {
                _listLines.Add(currentLine);
            }
            ParseLogData();

            // now sort by date & time
            SortLogEntries();

            TagWithGuardIDs();

            SortAndCheckGuardEntries();

            foreach (GuardData guard in _guardLogs)
            {
                Console.WriteLine($"{guard.GuardID} : slept {guard.MinutesAsleep} minutes total.");
            }

            ShowSleepiestGuard();

            CompareAllGuardsSleepMinutes();
        }

        private void SortLogEntries()
        {

            _logEntries  = _logEntries.OrderBy(time => time.TimeStamp).ToList();

            //foreach (LogEntry entry in _logEntries)
            //{
            //    Console.WriteLine($"[{entry.TimeStamp.Year}-{entry.TimeStamp.Month}-{entry.TimeStamp.Day} {entry.TimeStamp.Hour}:{entry.TimeStamp.Minute} {entry.GuardLog}");
            //}
        }


        //parses the guardlog and extracts the current guard's ID
        //and adds it to the entry's guardID
        //in cases where the guard is not listed in the entry,
        //give it the same guardID as the previous entry
        private void TagWithGuardIDs()
        {

            int currentGuardID = 0;

            foreach (LogEntry entry in _logEntries)
            {
                if (entry.GuardLog.Contains("Guard"))
                {
                    string[] idstring = entry.GuardLog.Split(' ');
                    string id = idstring[1].Trim('#');
                    Int32.TryParse(id, out currentGuardID);
                    entry.GuardID = currentGuardID;
                }
                else
                {
                    entry.GuardID = currentGuardID;
                }
                Console.WriteLine($"[{entry.TimeStamp.Year}-{entry.TimeStamp.Month}-{entry.TimeStamp.Day} {entry.TimeStamp.Hour}:{entry.TimeStamp.Minute} {entry.GuardID}");
            }
        }

        //checks the guard's sleep cycles and determines
        //the number of minutes they are asleep total
        private void SortAndCheckGuardEntries()
        {
            //Sort the Log Entries by minute
            List<LogEntry> guardTimeTable = _logEntries;

            //List<LogEntry> guardTimeTable = _logEntries.OrderBy(time => time.TimeStamp.Minute).ToList();

            GuardData currentGuard = new GuardData();


            //build a profile of each guard's sleep cycles
            //based on the sorted log entries
            //their sleep time is determined by their wake time
            //minus the shift start time
            foreach (LogEntry entry in guardTimeTable)
            {
                //search the guard entries for the current id
                //if it exists, then we'll work with that guard's data
                if (_guardLogs.Any(id => id.GuardID == entry.GuardID))
                {
                    currentGuard = _guardLogs.First(id => id.GuardID == entry.GuardID);
                }
                //otherwise create a new guard entry
                else
                {
                    GuardData newGuard = new GuardData();
                    newGuard.GuardID = entry.GuardID;
                    currentGuard = newGuard;
                    _guardLogs.Add(newGuard);
                }
                CheckGuardSleep(entry, currentGuard);
            }
        }

        //checks the guard's sleep cycles and determines
        //the number of minutes they are asleep total
        private void CheckGuardSleep(LogEntry entry, GuardData currentGuard)
        {
            //note when guard wakes up
            //and set their sleeping minutes
            if (entry.GuardLog.Contains("wakes"))
            {
                TimeSpan sleepyTime = entry.TimeStamp.Subtract(currentGuardSleepTime);
                currentGuard.MinutesAsleep += sleepyTime.Minutes;
                System.Console.WriteLine($"added {sleepyTime.Minutes}");
                SetSleepMinutes(currentGuard, currentGuardSleepTime, entry.TimeStamp);
            }
            //note when guard falls asleep
            else if (entry.GuardLog.Contains("asleep"))
            {
                Console.WriteLine($"guard {currentGuard.GuardID} fell asleep at {entry.TimeStamp.Minute}");
                currentGuardSleepTime = entry.TimeStamp;
                //TimeSpan wakeyTime = entry.TimeStamp.Subtract(currentGuardStartTime);
            }
            //note when the guard began shift
            else if (entry.GuardLog.Contains("begins"))
            {
                currentGuardStartTime = entry.TimeStamp;
            }
        }

        //Checks a specific guard's timetable to determine 
        //at what minute they are most likely to fall asleep
        private void SetSleepMinutes(GuardData guard, DateTime start, DateTime end)
        {
            for (int i = start.Minute; i <= end.Minute; i++)
            {
                Console.WriteLine($"#{guard.GuardID} started at: {start.Minute} ended at: {end.Minute} and incremented {i} by 1 minute");
                guard.MinuteByMinute[i]++;
            }
        }

        //search through the array for the guard's sleepiest minute
        //and return it as an integer
        private int CheckSleepMinutes(GuardData guard)
        {
            int currentMax = 0;
            int maxIndex = 0;

            for (int i=0; i < guard.MinuteByMinute.Length; i++)
            {
                if (currentMax < guard.MinuteByMinute[i])
                {
                    currentMax = guard.MinuteByMinute[i];
                    maxIndex = i;
                }
                Console.WriteLine($"{guard.MinuteByMinute[i]}");
            }
            Console.WriteLine($"The current max is {maxIndex}");
            return maxIndex;
        }

        //Finds the guard with the highest sleep minutes
        //and posts their stats
        private void ShowSleepiestGuard()
        {
            int currentMax = 0;
            GuardData sleepiestGuard = new GuardData();
            foreach (GuardData guard in _guardLogs)
            {
                if (currentMax < guard.MinutesAsleep)
                {
                    sleepiestGuard = guard;
                    currentMax = guard.MinutesAsleep;
                }
            }
            Console.WriteLine($"Sleepiest guard was {sleepiestGuard.GuardID} for a total of {sleepiestGuard.MinutesAsleep}");
            int sleepiestMinute = CheckSleepMinutes(sleepiestGuard);
            Console.WriteLine($"Guard was sleepiest at minute {sleepiestMinute} for {sleepiestGuard.MinuteByMinute[sleepiestMinute]}.");
        }

        //Check all guards for their sleepiest minute and compare them
        //Displays the guard who spent [minute] asleep more frequently
        //than anyone else
        private void CompareAllGuardsSleepMinutes()
        {
            int currentMax = 0;
            int maxIndex = 0;
            int maxGuardID = 0;

            for (int i = 0; i < 60; i++)
            {
                foreach (GuardData guard in _guardLogs)
                {
                    if (currentMax < guard.MinuteByMinute[i])
                    {
                        currentMax = guard.MinuteByMinute[i];
                        maxGuardID = guard.GuardID;
                        maxIndex = i;
                    }
                }
            }

            Console.WriteLine($"Guard #{maxGuardID} was asleep for {currentMax} minutes at minute {maxIndex}");
        }

        //split the raw log data into individual log entries
        private void ParseLogData()
        {
            foreach (string line in _listLines)
            {
                int year;
                int month;
                int day;
                int hours;
                int minutes;

                LogEntry newEntry = new LogEntry();

                //separate the datetime from the guard log
                string logEntryStamp = line.Substring(0, 17);

                //separate the date from the time
                string[] dateStamp = logEntryStamp.Split(' ');

                //trim off the leading bracket
                dateStamp[0] = dateStamp[0].Substring(1);

                //split the date into YYYYMMDD
                string[] dateSplit = dateStamp[0].Split('-');
                Int32.TryParse(dateSplit[0], out year);
                Int32.TryParse(dateSplit[1], out month);
                Int32.TryParse(dateSplit[2], out day);

                //extract the time
                string[] timeStamp = dateStamp[1].Split(':');
                Int32.TryParse(timeStamp[0], out hours);
                Int32.TryParse(timeStamp[1], out minutes);

                //extract the guard log information
                string guardLog = line.Substring(19);

                //build a log entry time stamp from the extracted raw file timestamp data
                newEntry.TimeStamp = new DateTime(year, month, day, hours, minutes, 0);
                newEntry.GuardLog = guardLog;

                _logEntries.Add(newEntry);
            }
        }
    }
}
