﻿using System;
using System.Collections.Generic;

namespace adventofcode
{
    public class assignment1
    {
        string line;
        int counter = 0;
        int dataloops = 0;
        int currentTotal;
        int? numberFound;
        List<int> listofNumbers;

        public assignment1()
        {
            listofNumbers = new List<int>();

            System.IO.StreamReader assignmentinput;

            while (numberFound == null)
            {
                assignmentinput = new System.IO.StreamReader(@"input/day1input.txt");
                while ((line = assignmentinput.ReadLine()) != null)
                {
                    int currentNumber;
                    Int32.TryParse(line, out currentNumber);
                    currentTotal += currentNumber;
                    listofNumbers.Add(currentTotal);
                    if (numberFound == null)
                        CheckArrayForRepetitions();
                    counter++;
                }
                assignmentinput.Close();
                dataloops++;
            }



            System.Console.WriteLine($"Parsed {counter} lines of input");
            System.Console.WriteLine($"The final output is {currentTotal}");
            System.Console.WriteLine($"The first repetition was {numberFound}");
            System.Console.WriteLine($"I had to loop through the data {dataloops} times!");
            //suspend screen
            System.Console.ReadLine();
        }

        private void CheckArrayForRepetitions()
        {
            for (int i=listofNumbers.Count - 2; i >= 0; i--)
            {
                //System.Console.WriteLine($"Trying {listofNumbers[counter]} vs {listofNumbers[i]}");
                if (listofNumbers[counter] == listofNumbers[i])
                    numberFound = listofNumbers[i];
            } 
        }
    }
}
