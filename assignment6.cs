﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace adventofcode
{
    public class assignment6
    {
        private List<string> _listLines = new List<string>();
        List<Point> _coordinates = new List<Point>();
        int[] _buildings;
        int[] _map; // stores characters representing each coordinate pair
        int _mapWidth;
        int _mapHeight;

        public assignment6()
        {
            string currentLine;
            //read raw text data from file
            //and push it into a list of strings
            System.IO.StreamReader assignmentinput;
            assignmentinput = new System.IO.StreamReader(@"input/day6input.txt");
            while ((currentLine = assignmentinput.ReadLine()) != null)
            {
                _listLines.Add(currentLine);
            }

            ParseStrings();

            InitBuildings();

            InitMap();

            Console.WriteLine($"{_map.Length}");
        }

        // grows each building until it 'bumps' into
        // the territory of a neighbouring building
        private void GrowAllBuildings()
        {
            //start by building a rectangle around the building
            int rectSize = 1;

            for (int i=0; i < _buildings.Length; i++)
            {
                //build the center of the building
                _map[(_coordinates[i].Y * _mapWidth + _coordinates[i].X)] = i;

                //build square around the building
                Rectangle buildingRect = new Rectangle(_coordinates[i].X, _coordinates[i].Y, rectSize, rectSize);
                {
                }
            }
        }

        // intializes the map array
        // according to the size of the largest
        // XY coordinates in the list
        private void InitMap()
        {
            int maxX = 0;
            int maxY = 0;

            for (int i=0; i < _coordinates.Count; i++)
            {
                if (_coordinates[i].X > maxX)
                    maxX = _coordinates[i].X;
                if (_coordinates[i].Y > maxY)
                    maxY = _coordinates[i].Y;
            }
            // make the map a bit larger to allow buildings for growth room
            _mapWidth = maxX + 50;
            _mapHeight = maxY + 50;
            _map = new int[_mapWidth * _mapHeight];
        }

        // initialize array of buildings and
        // populates them with their values
        // 1 for first building, 1 for second, and so on
        // 0 is reserved for mandatory "fences" between
        // neighbouring buildings
        private void InitBuildings()
        {
            _buildings = new int[_listLines.Count];
            int buildingID = 1;
            for (int i=0; i < _buildings.Length; i++)
            {
                _buildings[i] = buildingID;
                buildingID++;
            }
        }

        // chops up the input list of strings
        // into x/y coordinates stored as a list of Points
        private void ParseStrings()
        {
            foreach (string line in _listLines)
            {
                int x;
                int y;
                string[] splitLine = line.Split(',');
                Int32.TryParse(splitLine[0], out x);
                Int32.TryParse(splitLine[1].TrimStart(' '), out y); //chop off whitespace at the start
                Point newPoint = new Point(x, y);
                _coordinates.Add(newPoint);
            }
        }

        // 
    }
}
