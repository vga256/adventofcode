﻿using System;
using System.Drawing;
using System.Collections.Generic;

namespace adventofcode
{

    //Stores a list of clothing patterns
    //then creates a large map of these patterns, all overlaid
    //upon one another.

    public class assignment3
    {
        private List<string> _listLines = new List<string>();
        private List<PatternEntry> _listPatterns = new List<PatternEntry>();
        private int _intersectionArea;
        private int[] mapArray;
        private const int _mapWidth = 1000;
        private const int _mapHeight = 1000;

        //describes the cutting pattern of a piece of
        //santa claus' fabric
        //id: elfslave identification number
        //x: relative to left edge
        //y: relative to top edge
        //width: in inches
        //height: in inches
        public class PatternEntry
        {
            public int id;
            public int x;
            public int y;
            public int width;
            public int height;
            public Rectangle rect;
            public int numIntersections;
        }

        public assignment3()
        {
            mapArray = new int[_mapWidth * _mapHeight];

            string currentLine;

            //read raw text data from file
            //and push it into a list of strings
            System.IO.StreamReader assignmentinput;
            assignmentinput = new System.IO.StreamReader(@"input/day3input.txt");
            while ((currentLine = assignmentinput.ReadLine()) != null)
            {
                _listLines.Add(currentLine);
            }

            //parse the strings into structs
            parseLines();

            //Draw the map and then search for pattern overlaps
            DrawMap();
            ReadMapPatternOverlaps();

            //find the one pattern that didn't intersect with any other pattern
            CheckIntersections();

            Console.WriteLine($"The patterns create a total area of {CountTotalArea()} square inches.");

            //Console.WriteLine($"The patterns have a total of {numIntersections} intersections");
            Console.WriteLine($"The overlap is a total of {_intersectionArea} square inches.");
        }


        //Draw a map of all patterns using a 1D array
        //each time there is an overlap between patterns, increment
        //the map location by 1.
        private void DrawMap()
        { 
            foreach (PatternEntry pattern in _listPatterns)
            {
                for (int i = pattern.x; i < pattern.width + pattern.x; i++)
                {
                    for (int j = pattern.y; j < pattern.height + pattern.y; j++)
                    {
                        mapArray[(i * _mapWidth + j)]++;
                    }
                }
            }
        }

        //Read through the map and look for
        //places where the patterns overlapped at least
        //once. Places with no overlap are a "0". Places
        //with overlap begin at 1.
        private void ReadMapPatternOverlaps()
        {
            for (int i = 0; i < _mapWidth; i++)
            {
                for (int j = 0; j < _mapHeight; j++)
                {
                    if (mapArray[i * _mapWidth + j] > 1)
                        _intersectionArea++;
                }
            }
        }

        //Return the total area of all patterns
        //assuming they were laid side-to-side
        private int CountTotalArea()
        {
            int totalArea = 0;

            foreach (PatternEntry pattern in _listPatterns)
            {
                totalArea += (pattern.width * pattern.height);
            }

            return totalArea;
        }

        //checks the master list of patterns
        //to determine how many overlapping patterns there are
        private int CheckIntersections()
        {
            //count the number of intersections
            int intersections = 0;

            //loop through the list of patterns
            //and let each pattern store the number of intersections it has
            //with other rectangles *and* itself
            for (int i = 0; i < _listPatterns.Count; i++)
            {
                for (int j = 0; j < _listPatterns.Count; j++)
                {
                    if (_listPatterns[i].rect.IntersectsWith(_listPatterns[j].rect))
                    {
                        Rectangle intersectionRect = Rectangle.Intersect(_listPatterns[i].rect, _listPatterns[j].rect);
                        _listPatterns[i].numIntersections++;
                    }
                }
            }

            // now check how many times each pattern intersected
            //if it only intersected with itself, bingo!
            foreach (PatternEntry pattern in _listPatterns)
            {
                if (pattern.numIntersections == 1)
                {
                    Console.WriteLine($"ID# {pattern.id} didn't intersect with anyone else.");
                }
            }

            return intersections;
        }

        //parse each entry line and store all of the data
        //in an object that describes the id, x/y coords, and size
        private void parseLines()
        {
            foreach (string line in _listLines)
            {

                //temporarily store entry data
                //before pushing it into a new object
                int id;
                int x;
                int y;
                int width;
                int height;

                // split the entire entry into several strings
                string[] newString = line.Split(' ');

                //extract id
                Int32.TryParse(newString[0].Substring(1), out id);

                //parse coordinates
                string[] coordinates = newString[2].Split(',');
                //strip following colon off
                coordinates[1] = coordinates[1].Trim(':');

                //extract coordinates
                Int32.TryParse(coordinates[0], out x);
                Int32.TryParse(coordinates[1], out y);

                //extract width & height
                string[] size = newString[3].Split('x');
                Int32.TryParse(size[0], out width);
                Int32.TryParse(size[1], out height);

                PatternEntry newEntry = new PatternEntry();

                //push data into a new struct
                newEntry.id = id;
                newEntry.x = x;
                newEntry.y = y;
                newEntry.width = width;
                newEntry.height = height;
                newEntry.rect = new System.Drawing.Rectangle(x, y, width, height);
                newEntry.numIntersections = 0;

                _listPatterns.Add(newEntry);
            }
        }
    }
}
