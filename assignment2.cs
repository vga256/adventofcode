﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode
{
    public class assignment2
    {
        private List<string> _listLines = new List<string>();
        private int _twoCounter;
        private int _threeCounter;

        public assignment2()
        {

            string currentLine;

            System.IO.StreamReader assignmentinput;
            assignmentinput = new System.IO.StreamReader(@"input/day2input.txt");
            while ((currentLine = assignmentinput.ReadLine()) != null)
            {
                _listLines.Add(currentLine);
            }
            CompareStrings();
            System.Console.WriteLine($"Found {_twoCounter} sets of twos.");
            System.Console.WriteLine($"Found {_threeCounter} sets of threes.");
            System.Console.WriteLine($"Which makes for a total checksum of {_twoCounter * _threeCounter}");
            CompareLines();
        }

        private void CompareLines()
        {
            foreach (string line in _listLines)
            {
                HammingDistance(line);
            }
        }

        Func<string, string, int> hamming = (s1, s2) => s1.Zip(s2, (l, r) => l - r == 0 ? 0 : 1).Sum();

        private void HammingDistance(string s1)
        {
            int hammingThreshold = 1;

            var rs = _listLines
                .GroupBy(w => hamming(w, s1))
                .Where(h => h.Key <= hammingThreshold)
                .OrderByDescending(h => h.Key);

            foreach (var r in rs)
            {
                foreach (var g in r)
                    Console.Write(g + " ");
                Console.WriteLine("with distance " + r.Key);
            }
        }

        //compare each position of the string
        //with all other positions
        //and then count the number of times the character
        //repeats in the string
        private void CompareStrings()
        {
            int currLetterCount = 0;
            char[] letters = {'a', 'b', 'c','d','e','f','g','h','i','j','k',
                'l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
                


            foreach (string line in _listLines)
            {
                int lineDoubleCounter = 0;
                int lineTripleCounter = 0;

                char currentChar = new char();

                System.Console.WriteLine($"checking {line}");

                for (int i=0; i < letters.Length;i++)
                {
                    currLetterCount = 0;
                    currentChar = letters[i];
                    currLetterCount = line.Count(chr => chr == currentChar);
                    if (currLetterCount > 1)
                    {
                        System.Console.WriteLine($"Found {currLetterCount} of {currentChar}");
                        if (currLetterCount == 2)
                            lineDoubleCounter = 1;
                        else if (currLetterCount == 3)
                            lineTripleCounter = 1;
                    }
                }
                if (lineDoubleCounter > 0)
                    _twoCounter++;
                if (lineTripleCounter > 0)
                    _threeCounter++;
            }
        }
    }
}
