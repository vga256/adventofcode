﻿using System;
using System.Collections.Generic;

namespace adventofcode
{
    public class assignment5
    {
        private List<char> _polymer = new List<char>();
        private char[] _validUnitTypes = {'a', 'b', 'c','d','e','f','g','h','i','j','k',
                'l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        int lastPolymerSize = 0;
        int recursiveloops = 0;

        public assignment5()
        {
            char currentChar;
            //read raw text data from file
            //and push it into a list of strings
            System.IO.StreamReader assignmentinput;
            assignmentinput = new System.IO.StreamReader(@"input/day5input.txt");
            while (!assignmentinput.EndOfStream)
            {
                currentChar = (char)assignmentinput.Read();

                _polymer.Add(currentChar);
            }

            Console.WriteLine($"Initial polymer is {_polymer.Count}");
            lastPolymerSize = _polymer.Count + 1; // start the polymer counter
            // make a backup copy of the original unreacted polymer
            List<char> originalPolymer = new List<char>(_polymer);
            List<char> decomposedPolymer = PolymericReaction(_polymer);
            Console.WriteLine($"Final polymer is {decomposedPolymer.Count} which took {recursiveloops} loops");

            RemoveAndReactAllUnitTypes(originalPolymer);
        }

        // Recursively
        // dig through a polymer chain of AaBbCc - etc
        // each time there are neighbouring letters of OPPOSITE
        // case, remove them both
        private List<char> PolymericReaction(List<char> polymer)
        {
            //do not read to the very end
            // or very beginning of the polymer chain
            //as it will cause an out-of-bounds error
            if (lastPolymerSize > polymer.Count)
            {
                lastPolymerSize = polymer.Count;
                for (int i = 1; i < polymer.Count; i++)
                {
                    //check left neighbour for similarity
                    //uses RemoveAt so I don't accidentally screw
                    //with the List indexes as I remove stuff
                    char leftchar = InvertCase(polymer[i - 1]);
                    if (polymer[i] == leftchar)
                    {
                        polymer.RemoveAt(i);
                        polymer.RemoveAt(i - 1);
                    }
                }
                recursiveloops++;
                return PolymericReaction(polymer);
            }
            else
                return polymer;
        }

        // Reacts unit types one at a time from polymer
        // then runs a full polymeric reaction
        // and counts the unit type with the most efficient
        // reactions (i.e. producing the shortest polymeric chain)
        private void RemoveAndReactAllUnitTypes(List<char> polymer)
        {
            int smallestChain = polymer.Count;
            char mostEfficientUnitType = ' ';

            for (int i = 0; i < _validUnitTypes.Length; i++)
            {
                List<char> newPolymer = new List<char>(polymer);
                lastPolymerSize = polymer.Count + 1; // reset the polymer counter

                List<char> testUnitType = new List<char>();
                List<char> reducedPolymer = new List<char>();

                reducedPolymer = RemoveUnitFromPolymer(newPolymer, _validUnitTypes[i]);
                testUnitType = PolymericReaction(reducedPolymer);
                if (testUnitType.Count < smallestChain)
                {
                    smallestChain = testUnitType.Count;
                    mostEfficientUnitType = _validUnitTypes[i];
                }
                Console.WriteLine($"{_validUnitTypes[i]} had a reacted length of {testUnitType.Count}");
            }
            Console.WriteLine($"The most efficient single-unit-type reaction was {mostEfficientUnitType} which has a decomposed polymer of {smallestChain}");
        }

        //removes a specific unit type (char) from the
        //polymer and returns a new polymer list
        private List<char> RemoveUnitFromPolymer(List<char> polymer, char unitType)
        {
            for (int i = 0; i < polymer.Count; i++)
            {
                if ((polymer[i] == unitType) || polymer[i] == InvertCase(unitType))
                {
                    polymer.RemoveAt(i);
                    //Critical hack: step BACK the counter because we've just reduced the
                    //length of the polymer. This eliminates a bug where it skipped
                    //over items.
                    i--;
                }
            }
            return polymer;
        }

        // Converts an Uppercase character to a lowercase character
        // and lowercase character to Uppercase character
        private char InvertCase(char character)
        {
            if (char.IsLower(character))
                return char.ToUpper(character);
            else
                return char.ToLower(character);
        }
    }
}
